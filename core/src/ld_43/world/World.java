package ld_43.world;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import ld_43.entity.Entity;

public class World
{

	public static final float PIXELS_PER_METER = 10.0f;

	private List<Entity> Entities;
	private List<Entity> ForAdd;
	private com.badlogic.gdx.physics.box2d.World PhysicsWorld;

	public World()
	{
		Entities = new ArrayList<Entity>();
		ForAdd = new ArrayList<Entity>();

		PhysicsWorld = new com.badlogic.gdx.physics.box2d.World(new Vector2(0, 0 / PIXELS_PER_METER), false);
	}

	public com.badlogic.gdx.physics.box2d.World GetPhysicsWorld()
	{
		return PhysicsWorld;
	}

	public void AddEntity(Entity Ent)
	{
		ForAdd.add(Ent);
	}

	public <T extends Entity> List<T> FindObjectsByClass(Class<T> Class)
	{
		List<T> objects = new ArrayList<T>();
		Iterator<Entity> itr = Entities.iterator();

		while (itr.hasNext())
		{
			Entity next = itr.next();
			if (next.getClass() == Class)
				objects.add(Class.cast(next));
		}

		return objects;
	}

	public Body CreateBox(BodyType Type, float X, float Y, float W, float H, CollisionGroup Group)
	{

		BodyDef BDef = new BodyDef();
		BDef.fixedRotation = true;
		BDef.active = true;
		BDef.allowSleep = false;
		BDef.angle = 0.0f;
		BDef.angularDamping = 0.0f;
		BDef.angularVelocity = 0.0f;
		BDef.awake = true;
		BDef.bullet = false;
		BDef.linearDamping = 10.0f;
		BDef.type = Type;

		// The position of the body is the middle of the tile.
		BDef.position.set(X / PIXELS_PER_METER, Y / PIXELS_PER_METER);

		Body Body = GetPhysicsWorld().createBody(BDef);
		FixtureDef FDef = new FixtureDef();
		PolygonShape Box = new PolygonShape();
		Box.setAsBox((W / 2.0f) / PIXELS_PER_METER, (H / 2.0f) / PIXELS_PER_METER);
		FDef.density = 1.0f;
		FDef.friction = 0.0f;
		FDef.isSensor = false;
		FDef.restitution = 0.2f;
		FDef.shape = Box;

		FDef.filter.categoryBits = Group.getChannel();
		FDef.filter.groupIndex = Group.getGroup();
		FDef.filter.maskBits = Group.getAccepted();

		Body.createFixture(FDef);

		return Body;
	}

	public void Update(float Delta)
	{
		int Index = 0;

		// Step the physics world.
		PhysicsWorld.step(Delta, 6, 8);

		// Add pending entities.
		Entities.addAll(ForAdd);
		ForAdd.clear();

		// Update entities while removing entities marked for removal.
		while (Index < Entities.size())
		{
			Entity E = Entities.get(Index);

			if (E.IsDead())
			{
				Entities.remove(Index);
			}
			else
			{
				E.Update(Delta);
				Index++;
			}
		}

	}

	public void Render()
	{

		for (Entity E : Entities)
			E.Render();

	}

}
