package ld_43.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import ld_43.LD;
import ld_43.entity.Entity;
import ld_43.world.loader.tiled.MapObject;
import ld_43.world.loader.tiled.TiledMap;

public class TiledWorld extends World
{

	private TiledMap Map;
	private Map<Entity, List<HitListener>> HitListeners;
	private RayHandler RayHandler;
	private Body[] PhysicsBodies;
	private Box2DDebugRenderer dbgr;

	public TiledWorld(TiledMap Map, boolean[] Marked)
	{
		super();

		PhysicsBodies = new Body[Map.GetWidth() * Map.GetHeight()];

		// Set map.
		this.Map = Map;

		// Create entities
		this.HitListeners = new HashMap<Entity, List<HitListener>>();

		// Create Box2D World and light handler.
		this.RayHandler = new RayHandler(GetPhysicsWorld());
		RayHandler.setAmbientLight(0.00f);

		GetPhysicsWorld().setContactListener(new ContactListener()
		{
			@Override
			public void preSolve(Contact contact, Manifold oldManifold)
			{

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse)
			{

			}

			@Override
			public void endContact(Contact contact)
			{
				Fixture First = contact.getFixtureA();
				Fixture Second = contact.getFixtureA();
				Object FirstObj = First.getBody().getUserData();
				Object SecondObj = Second.getBody().getUserData();

				if (FirstObj instanceof Entity && SecondObj instanceof Entity)
				{
					Entity FirstEnt = (Entity) FirstObj;
					Entity SecondEnt = (Entity) FirstObj;
					List<HitListener> FirstEntListeners = HitListeners.get(FirstEnt);
					List<HitListener> SecondEntListeners = HitListeners.get(SecondEnt);

					if (FirstEntListeners != null)
						for (HitListener Listener : FirstEntListeners)
							Listener.OnHitEnd(SecondEnt);

					if (SecondEntListeners != null)
						for (HitListener Listener : SecondEntListeners)
							Listener.OnHitEnd(FirstEnt);
				}

			}

			@Override
			public void beginContact(Contact contact)
			{
				Fixture First = contact.getFixtureA();
				Fixture Second = contact.getFixtureB();
				Object FirstObj = First.getBody().getUserData();
				Object SecondObj = Second.getBody().getUserData();

				if (FirstObj instanceof Entity && SecondObj instanceof Entity)
				{
					Entity FirstEnt = (Entity) FirstObj;
					Entity SecondEnt = (Entity) SecondObj;
					List<HitListener> FirstEntListeners = HitListeners.get(FirstEnt);
					List<HitListener> SecondEntListeners = HitListeners.get(SecondEnt);

					if (FirstEntListeners != null)
						for (HitListener Listener : FirstEntListeners)
							Listener.OnHitBegin(SecondEnt);

					if (SecondEntListeners != null)
						for (HitListener Listener : SecondEntListeners)
							Listener.OnHitBegin(FirstEnt);

				}

			}
		});

		dbgr = new Box2DDebugRenderer();

		// Go through regions and add special properties.
		Iterator<String> ObjectItr = Map.GetObjectItr();
		CollisionGroup[] TileCollisionGroups = new CollisionGroup[Map.GetWidth() * Map.GetHeight()];

		// Fill tile collision groups with tiles
		for (int x = 0; x < Map.GetWidth(); x++)
			for (int y = 0; y < Map.GetHeight(); y++)
				TileCollisionGroups[x + y * Map.GetWidth()] = CollisionGroup.TILES;

		while (ObjectItr.hasNext())
		{
			String ObjectName = ObjectItr.next();
			List<MapObject> Objects = Map.GetObjects(ObjectName);
			for (MapObject Object : Objects)
			{
				int StartX = (int) (Object.GetRegion().x / Map.GetTileWidth());
				int StartY = (int) (Object.GetRegion().y / Map.GetTileHeight());
				int Width = (int) (Object.GetRegion().width / Map.GetTileWidth());
				int Height = (int) (Object.GetRegion().height / Map.GetTileHeight());

				if (ObjectName.equalsIgnoreCase("tilegroup"))
				{
					String Channel = Object.GetProperty("channel");

					for (int x = StartX; x <= StartX + Width; x++)
					{
						for (int y = StartY; y <= StartY + Height; y++)
						{
							Tile Tile = Map.GetLayer("Foreground").GetTile(x, y);
							if (Tile != null)
							{
								if (Channel.equalsIgnoreCase("pit"))
									TileCollisionGroups[x + y * Map.GetWidth()] = CollisionGroup.PIT;
								else
									System.out.println("Collision channel " + Channel + " not supported for tilegroups");
							}
						}
					}
				}
				else if (Object.GetName().equalsIgnoreCase("light"))
				{
					String Type = Object.GetProperty("type");
					String Color = Object.GetProperty("color");
					float Red = Float.parseFloat(Color.split(",")[0]);
					float Green = Float.parseFloat(Color.split(",")[1]);
					float Blue = Float.parseFloat(Color.split(",")[2]);
					float LightX = Map.GetTileWidth() * (StartX + Width / 2) / PIXELS_PER_METER;
					float LightY = Map.GetTileHeight() * ((Map.GetHeight() - StartY - 1) + Height / 2) / PIXELS_PER_METER;

					if (Type.equalsIgnoreCase("pit"))
					{
//						CreatePointLight(LightX, LightY, 150, new Color(Red, Green, Blue, 1.0f));
					}

				}

			}

		}

		// Create physics objects for all of the tiles on the foreground.
		Layer Foreground = Map.GetLayer("Foreground");
		for (int i = 0; i < Foreground.Width; i++)
		{
			for (int j = 0; j < Foreground.Height; j++)
			{
				Tile Tile = Foreground.Tiles[i + j * Foreground.Width];
				if (Tile != null && Marked[i + j * Foreground.Width])
				{
					float TileX = i * Tile.Width + Tile.Width / 2;
					float TileY = (Foreground.Height - j - 1) * Tile.Height + Tile.Height / 2;

					PhysicsBodies[i + j * Foreground.Width] = CreateBox(BodyType.StaticBody, TileX, TileY, Tile.Width, Tile.Height, TileCollisionGroups[i + j * Foreground.Width]);
				}
			}
		}
	}

	public void AddHitListener(Entity Ent, HitListener Listener)
	{
		List<HitListener> CurrentListeners = HitListeners.get(Ent);

		if (CurrentListeners == null)
			CurrentListeners = new ArrayList<HitListener>();

		CurrentListeners.add(Listener);
		HitListeners.put(Ent, CurrentListeners);
	}

	public void ClearForeground(int TileX, int TileY)
	{
		Layer Foreground = Map.GetLayer("Foreground");
		Tile Tile = Foreground.GetTile(TileX, TileY);

		if (Tile != null)
		{
			Body Body = PhysicsBodies[TileX + TileY * Foreground.Width];
			Body.destroyFixture(Body.getFixtureList().get(0));
			GetPhysicsWorld().destroyBody(Body);

			PhysicsBodies[TileX + TileY * Foreground.Width] = null;
		}

		Foreground.ClearTile(TileX, TileY);
	}

	public Location PixelLocToTileLoc(float X, float Y)
	{
		return Map.PixelLocToTileLoc(X, Y);
	}

	public TiledMap getMap()
	{
		return Map;
	}

	public PointLight CreatePitLight(float X, float Y, float Dist, Color Color)
	{
		PointLight Light = new PointLight(RayHandler, 50, Color, Dist / PIXELS_PER_METER, X, Y);

		Light.setSoft(true);
		Light.setStaticLight(true);
		Light.setSoftnessLength(1.0f);
		Light.setContactFilter(CollisionGroup.LIGHTS_PIT.getChannel(), CollisionGroup.LIGHTS_PIT.getGroup(), CollisionGroup.LIGHTS_PIT.getAccepted());

		return Light;
	}

	public PointLight CreatePointLight(float X, float Y, float Dist, Color Color)
	{
		return new PointLight(RayHandler, 1000, Color, Dist / PIXELS_PER_METER, X, Y);
	}

	public void Resize(int NewWidth, int NewHeight)
	{
	}

	public void Update(float Delta)
	{
		super.Update(Delta);

		// Time += Delta;
		// float Dist = 150 + 25 * (float) Math.sin(Time);
		// for (PointLight Light : PitLights)
		// Light.setDistance(Dist / PIXELS_PER_METER);
	}

	public void Render(OrthographicCamera camera)
	{
		
		Tile Tile = null;

		LD.GetBGBatch().setTransformMatrix(camera.view);
		LD.GetBGBatch().setProjectionMatrix(camera.projection);

		LD.GetBGBatch().begin();

		Iterator<String> LayerItr = Map.GetLayerIterator();

		// for (PointLight Light : PitLights)
		// {
		// float LightX = Light.getPosition().x * PIXELS_PER_METER;
		// float LightY = Light.getPosition().y * PIXELS_PER_METER;
		// float RSq = (float) Math.pow(LD.VIRTUAL_WIDTH / 2, 2) + (float)
		// Math.pow(LD.VIRTUAL_HEIGHT / 2, 2) + (float) Math.pow(2 * Light.getDistance()
		// * PIXELS_PER_METER, 2);
		// float DX = (LightX - camera.position.x);
		// float DY = (LightY - camera.position.y);
		//
		// if (DX * DX + DY * DY <= RSq)
		// Light.setActive(true);
		// else
		// Light.setActive(false);
		// }

		while (LayerItr.hasNext())
		{
			Layer Layer = Map.GetLayer(LayerItr.next());

			int StartX = (int) (camera.position.x - LD.VIRTUAL_WIDTH / 2) / Map.GetTileWidth() - 1;
			int StartY = (int) (camera.position.y - LD.VIRTUAL_HEIGHT / 2) / Map.GetTileHeight() - 1;
			int Width = LD.VIRTUAL_WIDTH / Map.GetTileWidth() + 3;
			int Height = LD.VIRTUAL_HEIGHT / Map.GetTileHeight() + 3;

			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					int AX = StartX + i;
					int AY = StartY + j;

					if (AX >= 0 && AX < Layer.Width && AY >= 0 && AY < Layer.Height)
					{
						Tile = Layer.GetTile(AX, Layer.Height-AY-1);

						if (Tile != null)
						{
							Tile.Drawable.draw(LD.GetBGBatch(), AX * Map.GetTileWidth(), AY * Map.GetTileHeight(), Tile.Width, Tile.Height);
						}
					}

				}
			}

		}

		// Render entities.
		super.Render();

		LD.GetBGBatch().end();

		Matrix4 Copy = new Matrix4(camera.combined);
		Copy.scale(PIXELS_PER_METER, PIXELS_PER_METER, 1);

		RayHandler.setCombinedMatrix(Copy, camera.position.x / PIXELS_PER_METER, camera.position.y / PIXELS_PER_METER, camera.viewportWidth * camera.zoom, camera.viewportHeight * camera.zoom);
		RayHandler.updateAndRender();

		// Render debug lines.
//		dbgr.render(GetPhysicsWorld(), Copy);

	}

}
