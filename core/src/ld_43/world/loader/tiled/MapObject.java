package ld_43.world.loader.tiled;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Rectangle;

public class MapObject
{

	private String Name;
	private Rectangle Region;
	private Map<String, String> Properties;

	public MapObject()
	{
		Properties = new HashMap<String, String>();
	}

	public MapObject(MapObject Other)
	{
		this.Name = Other.Name;
		this.Region = new Rectangle(Other.Region);
		this.Properties = new HashMap<String, String>();

		for (String Key : Other.Properties.keySet())
			Properties.put(Key, Other.Properties.get(Key));
	}

	public MapObject(String Name, Rectangle Region)
	{
		this();

		this.Name = Name;
		this.Region = Region;
	}

	public void AddProperty(String Name, String Value)
	{
		Properties.put(Name, Value);
	}

	public String GetProperty(String Name)
	{
		return Properties.get(Name);
	}

	public void SetRegion(Rectangle region)
	{
		Region = region;
	}

	public Rectangle GetRegion()
	{
		return Region;
	}

	public String GetName()
	{
		return Name;
	}

}
