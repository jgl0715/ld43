package ld_43.world.loader.tiled;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import ld_43.LD;
import ld_43.world.Layer;
import ld_43.world.Location;
import ld_43.world.Tile;
import ld_43.world.gen.RoomNode;

public class TiledMap
{

	private int Width;
	private int Height;
	private int TileWidth;
	private int TileHeight;

	private Map<String, Layer> Layers;
	private Map<Integer, Tile> Tiles;

	private Map<String, List<MapObject>> Objects;

	public TiledMap(int Width, int Height, int TileWidth, int TileHeight)
	{
		this.Width = Width;
		this.Height = Height;
		this.TileWidth = TileWidth;
		this.TileHeight = TileHeight;

		Layers = new HashMap<String, Layer>();
		Tiles = new HashMap<Integer, Tile>();
		Objects = new HashMap<String, List<MapObject>>();

	}

	public TiledMap(FileHandle MapFile)
	{

		Layers = new HashMap<String, Layer>();
		Tiles = new HashMap<Integer, Tile>();
		Objects = new HashMap<String, List<MapObject>>();

		// Parse Tiled XML file.

		XmlReader XMLReader = new XmlReader();

		Element Root = XMLReader.parse(MapFile);
		Width = Integer.parseInt(Root.get("width"));
		Height = Integer.parseInt(Root.get("height"));
		TileWidth = Integer.parseInt(Root.get("tilewidth"));
		TileHeight = Integer.parseInt(Root.get("tileheight"));

		Array<Element> TilesetElements = Root.getChildrenByName("tileset");
		for (int i = 0; i < TilesetElements.size; i++)
		{
			Element TilesetElement = TilesetElements.get(i);
			Element ImageElement = TilesetElement.getChildByName("image");

			String Path = ImageElement.getAttribute("source");
			int ImageWidth = Integer.parseInt(ImageElement.getAttribute("width"));
			int ImageHeight = Integer.parseInt(ImageElement.getAttribute("height"));

			// Remove the "." from the beginning
			Texture Texture = LD.GetTexture(Path.replace("..", "."));

			Array<Element> TileElements = TilesetElement.getChildrenByName("tile");
			int TilesInSet = Integer.parseInt(TilesetElement.getAttribute("tilecount"));
			int TSTileWidth = Integer.parseInt(TilesetElement.getAttribute("tilewidth"));
			int TSTileHeight = Integer.parseInt(TilesetElement.getAttribute("tileheight"));
			int TilesWide = ImageWidth / TSTileWidth;
			int TilesHigh = ImageHeight / TSTileHeight;

			for (int j = 0; j < TilesInSet; j++)
			{
				Tile Tile = new Tile();
				Tile.Drawable = new TextureRegionDrawable(new TextureRegion(Texture, (j % TilesWide) * TSTileWidth, (int) (j / TilesWide) * TileHeight, TSTileWidth, TSTileHeight));
				Tile.Width = TSTileWidth;
				Tile.Height = TSTileHeight;
				Tile.Properties = new HashMap<String, String>();

				Tiles.put(j, Tile);
			}

			for (int j = 0; j < TileElements.size; j++)
			{
				Element TileElement = TileElements.get(j);
				Element PropertiesElement = TileElement.getChildByName("properties");
				Array<Element> PropertyElements = PropertiesElement.getChildrenByName("property");

				int TileID = Integer.parseInt(TileElement.getAttribute("id")) - 1;
				Tile Tile = Tiles.get(TileID);

				if (Tile != null)
				{
					for (int k = 0; k < PropertyElements.size; k++)
					{
						Element PropertyElement = PropertyElements.get(k);
						String Name = PropertyElement.getAttribute("name");
						String Value = PropertyElement.getAttribute("value");

						Tile.Properties.put(Name, Value);
					}
				}
			}
		}

		// Load all the layers in.
		Array<Element> LayerElements = Root.getChildrenByName("layer");
		for (int i = 0; i < LayerElements.size; i++)
		{
			Element LayerElement = LayerElements.get(i);
			Element DataElement = LayerElement.getChildByName("data");
			Layer Layer = new Layer(LayerElement.get("name"), Integer.parseInt(LayerElement.get("width")), Integer.parseInt(LayerElement.get("height")));

			// Assuming XML data
			Array<Element> TileElements = DataElement.getChildrenByName("tile");
			for (int j = 0; j < Layer.Width; j++)
			{
				for (int k = 0; k < Layer.Height; k++)
				{
					int Index = j + k * Layer.Width;
					int TileIndex = Integer.parseInt(TileElements.get(Index).get("gid")) - 1;

					if (TileIndex < 0)
					{
						Layer.Tiles[Index] = null;
					}
					else
					{
						Layer.Tiles[Index] = Tiles.get(TileIndex);
					}
				}
			}

			Layers.put(Layer.Name, Layer);
		}

		Array<Element> ObjectGroupElements = Root.getChildrenByName("objectgroup");
		for (int i = 0; i < ObjectGroupElements.size; i++)
		{
			Element ObjectGroupElement = ObjectGroupElements.get(i);
			Array<Element> ObjectElements = ObjectGroupElement.getChildrenByName("object");

			for (int j = 0; j < ObjectElements.size; j++)
			{
				Element ObjectElement = ObjectElements.get(j);
				Element PropertiesElement = ObjectElement.getChildByName("properties");

				int ObjectID = Integer.parseInt(ObjectElement.get("id"));
				String ObjectName = ObjectElement.get("name");
				float O_X = Float.parseFloat(ObjectElement.get("x"));
				float O_Y = Float.parseFloat(ObjectElement.get("y"));

				float O_Width = Float.parseFloat(ObjectElement.get("width"));
				float O_Height = Float.parseFloat(ObjectElement.get("height"));

				// Get midpoint and translate to tile location.
				float MidX = O_X + O_Width / 2;
				float MidY = O_Y + O_Height / 2;
				Location TileLoc = PixelLocToTileLoc(MidX, MidY);
				TileLoc.Y = Height - TileLoc.Y - 1;

				MapObject Object = new MapObject(ObjectName, new Rectangle(O_X, O_Y, O_Width, O_Height));

				// Add object properties.
				if (PropertiesElement != null)
				{
					Array<Element> PropertyElements = PropertiesElement.getChildrenByName("property");

					for (int k = 0; k < PropertyElements.size; k++)
					{
						Element PropertyElement = PropertyElements.get(k);
						String Name = PropertyElement.get("name");
						String Value = PropertyElement.get("value");
						Object.AddProperty(Name, Value);

						if (Name.equalsIgnoreCase("flipy") && Boolean.parseBoolean(Value))
						{
							Object.GetRegion().y = GetPixelHeight() - Float.parseFloat(ObjectElement.get("y")) - 1;
						}
					}
				}

				AddObject(Object);
			}
		}
	}

	public void AddObject(MapObject Object)
	{
		List<MapObject> CurrentObjects = Objects.get(Object.GetName());
		if (CurrentObjects == null)
			CurrentObjects = new ArrayList<MapObject>();

		CurrentObjects.add(Object);
		Objects.put(Object.GetName(), CurrentObjects);
	}

	public Iterator<String> GetObjectItr()
	{
		return Objects.keySet().iterator();
	}

	public List<MapObject> GetObjects(String Name)
	{
		return Objects.get(Name);
	}

	public float GetPixelWidth()
	{
		return Width * TileWidth;
	}

	public float GetPixelHeight()
	{
		return Height * TileHeight;
	}

	public void AddTile(Tile Tile, int ID)
	{
		if (!Tiles.containsKey(ID))
		{
			TextureRegion tr = ((TextureRegionDrawable) Tile.Drawable).getRegion();

			Tiles.put(ID, Tile);
		}
	}

	public Tile GetTile(int ID)
	{
		return Tiles.get(ID);
	}

	public Iterator<Integer> GetTileIDIterator()
	{
		return Tiles.keySet().iterator();
	}

	public Iterator<String> GetLayerIterator()
	{
		return Layers.keySet().iterator();
	}

	public void Paste(TiledMap Other, int StartX, int StartY)
	{
		// Transfer tiles from all layers over
		Iterator<String> LayerItr = Other.GetLayerIterator();
		Iterator<Integer> TileItr = Other.GetTileIDIterator();
		Iterator<String> ObjectItr = Other.GetObjectItr();

		while (LayerItr.hasNext())
		{
			String LayerName = LayerItr.next();
			Layer OtherLayer = Other.GetLayer(LayerName);
			Layer ThisLayer = GetLayer(LayerName);

			if (ThisLayer != null)
			{

				// Copy over tiles from layer.
				for (int i = 0; i < Other.Width; i++)
				{
					for (int j = 0; j < Other.Height; j++)
					{
						ThisLayer.SetTile(StartX + i, StartY + j, OtherLayer.GetTile(i, j));
					}
				}

			}
		}

		// Transfer all tiles from room map to this map.
		while (TileItr.hasNext())
		{
			int TileID = TileItr.next();
			Tile OtherTile = Other.GetTile(TileID);

			AddTile(OtherTile, TileID);
		}

		// Transfer over map objects.
		while (ObjectItr.hasNext())
		{
			List<MapObject> Objects = Other.GetObjects(ObjectItr.next());

			for (MapObject Object : Objects)
			{
				MapObject Copy = new MapObject(Object);
				// System.out.println("Before: x=" + Copy.GetRegion().x + " y=" +
				// Copy.GetRegion().y);

				// Translate objects into world space.
				Copy.GetRegion().x += StartX * TileWidth;
				Copy.GetRegion().y += StartY * TileHeight;

				// System.out.println("Moved object: x=" + Copy.GetRegion().x + " y=" +
				// Copy.GetRegion().y);

				AddObject(Copy);
			}
		}

	}

	public Location PixelLocToTileLoc(float X, float Y)
	{
		int TX = (int) (X / TileWidth);
		int TY = (int) (Y / TileHeight);

		return new Location(TX, TY);
	}

	public void AddLayer(Layer Layer)
	{
		Layers.put(Layer.Name, Layer);
	}

	public Layer GetLayer(String Name)
	{
		return Layers.get(Name);
	}

	public int GetWidth()
	{
		return Width;
	}

	public int GetHeight()
	{
		return Height;
	}

	public int GetTileWidth()
	{
		return TileWidth;
	}

	public int GetTileHeight()
	{
		return TileHeight;
	}

}
