package ld_43.world;

import java.util.Map;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class Tile
{

	public int Width;
	public int Height;
	public Drawable Drawable;
	public Map<String, String> Properties;

}
