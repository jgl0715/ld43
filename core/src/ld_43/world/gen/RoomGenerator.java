package ld_43.world.gen;

import java.util.Random;

import ld_43.world.loader.tiled.TiledMap;

public abstract class RoomGenerator
{

	public abstract TiledMap GenRooms(Random Rand, RoomNode Partition, boolean[] Marked);

}
