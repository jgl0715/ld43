package ld_43.world.gen;

import java.util.Random;

public abstract class Partitioner
{

	public abstract void Partition(Random Rand, RoomNode Root);

}
