package ld_43.world.gen;

import java.util.Random;

import ld_43.world.TiledWorld;

public abstract class Populator
{
	
	public abstract void Populate(Random Rand, TiledWorld Level, RoomNode Room);

}
