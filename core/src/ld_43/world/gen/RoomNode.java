package ld_43.world.gen;

import java.util.ArrayList;
import java.util.List;

import ld_43.item.Item;
import ld_43.world.Location;
import ld_43.world.TiledWorld;

public class RoomNode
{

	public float X;
	public float Y;
	public float Width;
	public float Height;

	public int RoomX;
	public int RoomY;
	public int RoomW;
	public int RoomH;

	public RoomNode Left;
	public RoomNode Right;
	public RoomNode Parent;

	public List<Location> Entrances;
	public List<Item> ItemsNeeded;

	private boolean Complete;

	public RoomNode(float X, float Y, float W, float H)
	{
		this.X = X;
		this.Y = Y;
		this.Width = W;
		this.Height = H;
		this.Complete = false;

		Entrances = new ArrayList<Location>();
		ItemsNeeded = new ArrayList<Item>();
	}

	public void Complete(TiledWorld World)
	{
		Complete = true;

		if (Parent != null)
		{
			if (this == Parent.Left)
			{
				if (Parent.Right == null || Parent.Right.IsComplete())
				{
					System.out.println("completing parent");
					Parent.Complete(World);
				}
			}
			else
			{
				if (Parent.Left == null || Parent.Left.IsComplete())
				{
					System.out.println("completing parent");
					Parent.Complete(World);

				}
			}

			// Open up right sibling room.
			if (Parent.Right != null)
				Parent.Right.OpenEntry(World);
		}

		if (IsLeaf())
			OpenEntry(World);

	}

	public void OpenEntry(TiledWorld World)
	{
		if (!IsLeaf())
		{
			Left.OpenEntry(World);
			Right.OpenEntry(World);
		}
		else
		{
			for (Location Loc : Entrances)
			{
				World.ClearForeground(Loc.X, Loc.Y);
			}
		}
	}

	public boolean IsComplete()
	{
		return Complete;
	}

	public boolean IsLeaf()
	{
		return Left == null && Right == null;
	}
}
