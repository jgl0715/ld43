package ld_43.world.gen;

import ld_43.world.loader.tiled.TiledMap;

public abstract class Stitcher
{

	public abstract void Stitch(TiledMap World, RoomNode Root, boolean[] Marked);

}
