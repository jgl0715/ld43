package ld_43.world.gen.def;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import ld_43.world.Layer;
import ld_43.world.Location;
import ld_43.world.gen.RoomNode;
import ld_43.world.gen.Stitcher;
import ld_43.world.loader.tiled.TiledMap;

public class DefStitcher extends Stitcher
{

	public static final int HALL_SIZE = 2;
	public static final int HALL_BG = 144;

	public static final int HALL_LEFT = 38;
	public static final int HALL_RIGHT = 39;
	public static final int HALL_TOP_LEFT = 63;
	public static final int HALL_TOP_RIGHT = 60;
	public static final int HALL_BOTTOM_RIGHT_1 = 76;
	public static final int HALL_BOTTOM_RIGHT_2 = 92;
	public static final int HALL_BOTTOM_RIGHT_3 = 108;
	public static final int HALL_BOTTOM_LEFT_1 = 79;
	public static final int HALL_BOTTOM_LEFT_2 = 95;
	public static final int HALL_BOTTOM_LEFT_3 = 111;

	public static final int HALL_TOP_1 = 45;
	public static final int HALL_TOP_2 = 29;
	public static final int HALL_TOP_3 = 13;
	public static final int HALL_BOTTOM = 61;
	public static final int HALL_LEFT_BUTTOM = 60;
	public static final int HALL_RIGHT_BOTTOM = 63;

	private boolean OverlapsX(RoomNode First, RoomNode Second)
	{
		if (First.RoomX + First.RoomW - 1 >= Second.RoomX && First.RoomX + First.RoomW - 1 < Second.RoomX + Second.RoomW)
			return true;
		if (Second.RoomX + Second.RoomW - 1 >= First.RoomX && Second.RoomX + Second.RoomW - 1 < First.RoomX + First.RoomW)
			return true;

		return false;
	}

	private boolean OverlapsY(RoomNode First, RoomNode Second)
	{
		if (First.RoomY + First.RoomH - 1 >= Second.RoomY && First.RoomY + First.RoomH - 1 < Second.RoomY + Second.RoomH)
			return true;
		if (Second.RoomY + Second.RoomH - 1 >= First.RoomY && Second.RoomY + Second.RoomH - 1 < First.RoomY + First.RoomH)
			return true;
		return false;
	}

	/**
	 * Note: assumes that rooms are overlapping on the x axis.
	 * 
	 * @param First
	 * @param Second
	 * @return
	 */
	private int OverlapX(RoomNode First, RoomNode Second)
	{
		if (First.RoomX > Second.RoomX)
		{
			return First.RoomX;
		} else
		{
			return Second.RoomX;
		}
	}

	/**
	 * Note: assumes that rooms are overlapping on the x axis.
	 * 
	 * @param First
	 * @param Second
	 * @return
	 */
	private int OverlapWidth(RoomNode First, RoomNode Second)
	{
		if (First.RoomX > Second.RoomX)
		{
			return Second.RoomX + Second.RoomW - 1 - First.RoomX;
		} else
		{
			return First.RoomX + First.RoomW - 1 - Second.RoomX;
		}
	}

	/**
	 * Note: assumes that rooms are overlapping on the y axis.
	 * 
	 * @param First
	 * @param Second
	 * @return
	 */
	private int OverlapY(RoomNode First, RoomNode Second)
	{
		if (First.RoomY > Second.RoomY)
		{
			return First.RoomY;
		} else
		{
			return Second.RoomY;
		}
	}

	/**
	 * Note: assumes that rooms are overlapping on the y axis.
	 * 
	 * @param First
	 * @param Second
	 * @return
	 */
	private int OverlapHeight(RoomNode First, RoomNode Second)
	{
		if (First.RoomY > Second.RoomY)
		{
			return Second.RoomY + Second.RoomH - 1 - First.RoomY;
		} else
		{
			return First.RoomY + First.RoomH - 1 - Second.RoomY;
		}
	}

	private Stack<RoomNode> BFSStack(RoomNode Root)
	{
		// Do BFS of RoomNodes
		Queue<RoomNode> Nodes = new LinkedList<RoomNode>();
		Stack<RoomNode> Stack = new Stack<RoomNode>();

		Nodes.offer(Root);

		while (!Nodes.isEmpty())
		{
			RoomNode Node = Nodes.poll();
			RoomNode Left = Node.Left;
			RoomNode Right = Node.Right;

			if (Left != null && Right != null)
			{
				Nodes.offer(Left);
				Nodes.offer(Right);
			}

			Stack.push(Node);
		}

		return Stack;
	}

	private void ConnectSections(Layer FG, Layer BG, TiledMap Map, RoomNode First, RoomNode Second, boolean[] Marked)
	{
		Stack<RoomNode> BFSFirst = BFSStack(First);
		Stack<RoomNode> BFSSecond = BFSStack(Second);
		List<RoomNode> FirstNodes = new ArrayList<RoomNode>();
		List<RoomNode> SecondNodes = new ArrayList<RoomNode>();

		while (!BFSFirst.isEmpty())
			FirstNodes.add(BFSFirst.pop());

		while (!BFSSecond.isEmpty())
			SecondNodes.add(BFSSecond.pop());

		RoomNode MinOne = null, MinTwo = null;
		float MinDistance = Float.MAX_VALUE;

		for (int i = 0; i < FirstNodes.size(); i++)
		{
			RoomNode FirstNode = FirstNodes.get(i);
			float FirstMidX = FirstNode.RoomX + FirstNode.RoomW / 2;
			float FirstMidY = FirstNode.RoomY + FirstNode.RoomH / 2;

			if (FirstNode.IsLeaf())
			{
				for (int j = 0; j < SecondNodes.size(); j++)
				{
					RoomNode SecondNode = SecondNodes.get(j);
					if (SecondNode.IsLeaf())
					{
						float DX = FirstMidX - (SecondNode.RoomX + SecondNode.RoomW / 2);
						float DY = FirstMidY - (SecondNode.RoomY + SecondNode.RoomH / 2);
						float Dist = (float) Math.sqrt(DX * DX + DY * DY);

						if (Dist < MinDistance)
						{
							MinDistance = Dist;
							MinOne = FirstNode;
							MinTwo = SecondNode;
						}
					}
				}
			}

		}

		ConnectLeafs(FG, BG, Map, MinOne, MinTwo, Marked);

	}

	private void ConnectLeaf(Layer FG, Layer BG, TiledMap Map, RoomNode Leaf, RoomNode Section, boolean[] Marked)
	{
		Stack<RoomNode> BFS = BFSStack(Section);

		RoomNode MinLeaf = null;
		float MinDistance = Float.MAX_VALUE;

		float MidX = Leaf.RoomX + Leaf.RoomW / 2;
		float MidY = Leaf.RoomY + Leaf.RoomH / 2;

		while (!BFS.isEmpty())
		{
			RoomNode Node = BFS.pop();

			if (Node.IsLeaf())
			{
				float DX = MidX - (Node.RoomX + Node.RoomW / 2);
				float DY = MidY - (Node.RoomY + Node.RoomH / 2);
				float Dist = (float) Math.sqrt(DX * DX + DY * DY);

				if (Dist < MinDistance)
				{
					MinDistance = Dist;
					MinLeaf = Node;
				}

			}
		}

		// Connect the leafs to connect the leaf to the tree
		ConnectLeafs(FG, BG, Map, MinLeaf, Leaf, Marked);

	}

	private void ConnectLeafs(Layer FG, Layer BG, TiledMap Map, RoomNode Left, RoomNode Right, boolean[] Marked)
	{
		if (OverlapsX(Left, Right))
		{
			if (OverlapWidth(Left, Right) <= HALL_SIZE + 1)
				ConnectMidpoints(FG, BG, Map, Left, Right, Marked);
			else
				ConnectX(FG, BG, Map, Left, Right, Marked);
		} else if (OverlapsY(Left, Right))
		{

			if (OverlapHeight(Left, Right) <= HALL_SIZE + 1)
				ConnectMidpoints(FG, BG, Map, Left, Right, Marked);
			else
				ConnectY(FG, BG, Map, Left, Right, Marked);
		} else
		{
			ConnectMidpoints(FG, BG, Map, Left, Right, Marked);
		}
	}

	private void ConnectX(Layer FG, Layer BG, TiledMap Map, RoomNode Left, RoomNode Right, boolean[] Marked)
	{
		int OverlapX = OverlapX(Left, Right);
		int OverlapW = OverlapWidth(Left, Right);
		int HallStartX = OverlapX + OverlapW / 2;

		int HallStartY;
		int HallHeight;

		if (Left.Y > Right.Y)
		{
			HallStartY = Right.RoomY + Right.RoomH - 1;
			HallHeight = Left.RoomY - HallStartY + 4;

			for (int i = 0; i < HALL_SIZE; i++)
			{
				Left.Entrances.add(new Location(HallStartX + i, HallStartY + HallHeight - 2));
				Right.Entrances.add(new Location(HallStartX + i, HallStartY));
			}

		} else
		{
			HallStartY = Left.RoomY + Left.RoomH - 1;
			HallHeight = Right.RoomY - HallStartY + 4;

			for (int i = 0; i < HALL_SIZE; i++)
			{
				Left.Entrances.add(new Location(HallStartX + i, HallStartY));
				Right.Entrances.add(new Location(HallStartX + i, HallStartY + HallHeight - 2));
			}

		}

		// Put four corner turning halls
		TopLeftHall(BG, FG, Map, HallStartX - 1, HallStartY, Marked);
		TopRightHall(BG, FG, Map, HallStartX + HALL_SIZE, HallStartY, Marked);
		BottomLeftHall(BG, FG, Map, HallStartX - 1, HallStartY + HallHeight - 4, Marked);
		BottomRightHall(BG, FG, Map, HallStartX + HALL_SIZE, HallStartY + HallHeight - 4, Marked);

		// Line the halls edges with the proper corner tiles.
		for (int j = HallStartY + 1; j < HallStartY + HallHeight - 3; j++)
		{
			LeftHall(BG, FG, Map, HallStartX - 1, j, Marked);
			RightHall(BG, FG, Map, HallStartX + HALL_SIZE, j, Marked);
		}

		for (int i = HallStartX; i < HallStartX + HALL_SIZE; i++)
		{
			for (int j = HallStartY; j < HallStartY + HallHeight; j++)
			{
				Hall(BG, FG, Map, i, j, Marked);
			}
		}

		for (Location Loc : Left.Entrances)
			Entrance(BG, FG, Map, Loc.X, Loc.Y, Marked);

		for (Location Loc : Right.Entrances)
			Entrance(BG, FG, Map, Loc.X, Loc.Y, Marked);

	}

	private void ConnectY(Layer FG, Layer BG, TiledMap Map, RoomNode Left, RoomNode Right, boolean[] Marked)
	{
		int OverlapY = OverlapY(Left, Right);
		int OverlapH = OverlapHeight(Left, Right);
		int HallStartY = OverlapY + OverlapH / 2;

		int HallStartX;
		int HallWidth;

		if (Left.X > Right.X)
		{
			HallStartX = Right.RoomX + Right.RoomW - 1;
			HallWidth = Left.RoomX - HallStartX + 4;

			for (int i = 0; i < HALL_SIZE; i++)
			{
				Left.Entrances.add(new Location(HallStartX + HallWidth - 2, HallStartY + i));
				Right.Entrances.add(new Location(HallStartX, HallStartY + i));
			}

		} else
		{
			HallStartX = Left.RoomX + Left.RoomW - 1;
			HallWidth = Right.RoomX - HallStartX + 4;

			for (int i = 0; i < HALL_SIZE; i++)
			{
				Left.Entrances.add(new Location(HallStartX, HallStartY + i));
				Right.Entrances.add(new Location(HallStartX + HallWidth - 2, HallStartY + i));
			}
		}

		LeftTopHall(BG, FG, Map, HallStartX, HallStartY - 1, Marked);
		LeftBottomHall(BG, FG, Map, HallStartX, HallStartY + HALL_SIZE, Marked);
		RightTopHall(BG, FG, Map, HallStartX + HallWidth - 4, HallStartY - 1, Marked);
		RightBottomHall(BG, FG, Map, HallStartX + HallWidth - 4, HallStartY + HALL_SIZE, Marked);

		for (int i = HallStartX + 1; i < HallStartX + HallWidth - 4; i++)
		{
			TopHall(BG, FG, Map, i, HallStartY - 1, Marked);
			BottomHall(BG, FG, Map, i, HallStartY + HALL_SIZE, Marked);
		}

		for (int i = HallStartX; i < HallStartX + HallWidth - 3; i++)
		{
			for (int j = HallStartY; j < HallStartY + HALL_SIZE; j++)
			{
				Hall(BG, FG, Map, i, j, Marked);
			}
		}

		for (Location Loc : Left.Entrances)
			Entrance(BG, FG, Map, Loc.X, Loc.Y, Marked);

		for (Location Loc : Right.Entrances)
			Entrance(BG, FG, Map, Loc.X, Loc.Y, Marked);

	}

	private void ConnectMidpoints(Layer FG, Layer BG, TiledMap Map, RoomNode First, RoomNode Second, boolean[] Marked)
	{
		RoomNode Left = null;
		RoomNode Right = null;
		if (First.RoomX < Second.RoomX)
		{
			Left = First;
			Right = Second;
		} else
		{
			Left = Second;
			Right = First;
		}

		int LeftX = Left.RoomX + Left.RoomW - 1;
		int LeftY = Left.RoomY + Left.RoomH / 2 - HALL_SIZE / 2;
		int RightMidX = (Right.RoomX + Right.RoomW / 2);
		int HorHallWidth = RightMidX - LeftX;

		for (int i = RightMidX - HALL_SIZE / 2 - 1; i < RightMidX + HALL_SIZE / 2; i++)
		{
			int StartY;
			int EndY;

			if (Left.RoomY < Right.RoomY)
			{
				StartY = LeftY;
				EndY = Right.RoomY + 3;
			} else
			{
				StartY = Right.RoomY + Right.RoomH - 1;
				EndY = LeftY;
			}

			for (int j = StartY; j <= EndY; j++)
				Hall(BG, FG, Map, i, j, Marked);

		}

		for (int i = LeftX; i <= LeftX + HorHallWidth; i++)
		{
			for (int j = LeftY; j < LeftY + HALL_SIZE; j++)
			{
				Hall(BG, FG, Map, i, j, Marked);
			}
		}

	}

	private void Entrance(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
	}

	private void Hall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.ClearTile(X, Y);
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));
	}

	private void LeftHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_LEFT));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));
		Mark(X, Y, BG, Marked);
	}

	private void RightHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_RIGHT));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));
		Mark(X, Y, BG, Marked);

	}

	private void TopHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_TOP_1));
		FG.SetTile(X, Y - 1, Map.GetTile(HALL_TOP_2));
		FG.SetTile(X, Y - 2, Map.GetTile(HALL_TOP_3));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);

	}

	private void BottomHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_BOTTOM));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));
		Mark(X, Y, BG, Marked);

	}

	private void TopLeftHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_TOP_LEFT));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);

	}

	private void TopRightHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_TOP_RIGHT));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
	}

	private void BottomLeftHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_BOTTOM_LEFT_1));
		FG.SetTile(X, Y + 1, Map.GetTile(HALL_BOTTOM_LEFT_2));
		FG.SetTile(X, Y + 2, Map.GetTile(HALL_BOTTOM_LEFT_3));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
		Mark(X, Y + 1, BG, Marked);
		Mark(X, Y + 2, BG, Marked);
	}

	private void BottomRightHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_BOTTOM_RIGHT_1));
		FG.SetTile(X, Y + 1, Map.GetTile(HALL_BOTTOM_RIGHT_2));
		FG.SetTile(X, Y + 2, Map.GetTile(HALL_BOTTOM_RIGHT_3));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
		Mark(X, Y + 1, BG, Marked);
		Mark(X, Y + 2, BG, Marked);
	}

	private void LeftTopHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_BOTTOM_RIGHT_3));
		FG.SetTile(X, Y - 1, Map.GetTile(HALL_BOTTOM_RIGHT_2));
		FG.SetTile(X, Y - 2, Map.GetTile(HALL_BOTTOM_RIGHT_1));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
		Mark(X, Y - 1, BG, Marked);
		Mark(X, Y - 2, BG, Marked);
	}

	private void LeftBottomHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_LEFT_BUTTOM));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);

	}

	private void RightTopHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_BOTTOM_LEFT_3));
		FG.SetTile(X, Y - 1, Map.GetTile(HALL_BOTTOM_LEFT_2));
		FG.SetTile(X, Y - 2, Map.GetTile(HALL_BOTTOM_LEFT_1));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
		Mark(X, Y - 1, BG, Marked);
		Mark(X, Y - 2, BG, Marked);
	}

	private void RightBottomHall(Layer BG, Layer FG, TiledMap Map, int X, int Y, boolean[] Marked)
	{
		FG.SetTile(X, Y, Map.GetTile(HALL_RIGHT_BOTTOM));
		BG.SetTile(X, Y, Map.GetTile(HALL_BG));

		Mark(X, Y, BG, Marked);
	}

	public void Mark(int X, int Y, Layer Layer, boolean[] Marked)
	{
		Marked[X + Y * Layer.Width] = true;
	}

	@Override
	public void Stitch(TiledMap Map, RoomNode Root, boolean[] Marked)
	{
		Layer FG = Map.GetLayer("Foreground");
		Layer BG = Map.GetLayer("Background");

		Stack<RoomNode> BFS = BFSStack(Root);

		while (!BFS.isEmpty())
		{
			if (BFS.size() == 1)
			{
				BFS.pop();
			} else
			{
				RoomNode Right = BFS.pop();
				RoomNode Left = BFS.pop();

				if (Left.IsLeaf() && Right.IsLeaf())
					ConnectLeafs(FG, BG, Map, Left, Right, Marked);
				else if (Left.IsLeaf() && !Right.IsLeaf())
					ConnectLeaf(FG, BG, Map, Left, Right, Marked);
				else if (Right.IsLeaf() && !Left.IsLeaf())
					ConnectLeaf(FG, BG, Map, Right, Left, Marked);
				else
					ConnectSections(FG, BG, Map, Left, Right, Marked);

			}

		}
	}

}
