package ld_43.world.gen.def;

import java.util.Random;

import ld_43.world.gen.Partitioner;
import ld_43.world.gen.RoomNode;

public class DefPartitioner extends Partitioner
{

	public static final int MIN_DIM = 30;
	public static final int MAX_DIM = 15;

	public float RandFloat(Random Rand, float Min, float Max)
	{
		return Rand.nextFloat() * (Max - Min) + Min;
	}

	@Override
	public void Partition(Random Rand, RoomNode Root)
	{
		float HRange = Root.Width;
		float VRange = Root.Height;
		boolean HAxisGood = false;
		boolean VAxisGood = false;

		if (HRange < MIN_DIM)
			throw new IllegalStateException("HRange was < MIN_DIM. Try making the world size bigger?");
		if (VRange < MIN_DIM)
			throw new IllegalStateException("VRange was < MIN_DIM. Try making the world size bigger?");

		if (HRange >= 2 * MIN_DIM)
			HAxisGood = true;
		if (VRange >= 2 * MIN_DIM)
			VAxisGood = true;

		if (HAxisGood && VAxisGood)
		{
			if (Rand.nextBoolean())
				SplitVertical(Rand, Root);
			else
				SplitHorizontal(Rand, Root);
		} else if (HAxisGood && !VAxisGood)
		{
			SplitVertical(Rand, Root);
		} else if (VAxisGood && !HAxisGood)
		{
			SplitHorizontal(Rand, Root);
		}

		// Otherwise we can't divide either axis
	}

	private void SplitVertical(Random Rand, RoomNode Root)
	{
		float Cut = RandFloat(Rand, Root.X + MIN_DIM, Root.X + Root.Width - MIN_DIM);

		RoomNode Left = new RoomNode(Root.X, Root.Y, Cut - Root.X, Root.Height);
		RoomNode Right = new RoomNode(Cut, Root.Y, Root.X + Root.Width - Cut, Root.Height);

		Left.Parent = Root;
		Right.Parent = Root;
		Root.Left = Left;
		Root.Right = Right;

		Partition(Rand, Left);
		Partition(Rand, Right);
	}

	private void SplitHorizontal(Random Rand, RoomNode Root)
	{
		float Cut = RandFloat(Rand, Root.Y + MIN_DIM, Root.Y + Root.Height - MIN_DIM);

		RoomNode Bottom = new RoomNode(Root.X, Root.Y, Root.Width, Cut - Root.Y);
		RoomNode Top = new RoomNode(Root.X, Cut, Root.Width, Root.Y + Root.Height - Cut);

		Bottom.Parent = Root;
		Top.Parent = Root;
		Root.Left = Bottom;
		Root.Right = Top;

		Partition(Rand, Bottom);
		Partition(Rand, Top);
	}

}
