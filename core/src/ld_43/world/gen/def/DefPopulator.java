package ld_43.world.gen.def;

import java.util.Random;

import ld_43.world.Layer;
import ld_43.world.TiledWorld;
import ld_43.world.gen.Populator;
import ld_43.world.gen.RoomNode;
import ld_43.world.loader.tiled.TiledMap;

public class DefPopulator extends Populator
{

	public boolean IsClear(TiledMap Map, int X, int Y)
	{
		boolean IsClear = true;

		Layer Foreground = Map.GetLayer("Foreground");
		if (Foreground.GetTile(X, Y) != null)
			IsClear = false;

		return IsClear;

	}

	@Override
	public void Populate(Random Rand, TiledWorld Level, RoomNode Room)
	{
		if (Room.IsLeaf())
		{
			int StartX = Room.RoomX;
			int StartY = Room.RoomY;
			int EndX = StartX + Room.RoomW;
			int EndY = StartY + Room.RoomH;

			for (int x = StartX; x < EndX; x++)
			{
				for (int y = StartY; y < EndY; y++)
				{
					if (IsClear(Level.getMap(), x, y) && Rand.nextFloat() < 0.1f)
					{
//						ItemEntity Ent = new ItemEntity(Level, x * Level.getMap().GetTileWidth(), (Level.getMap().GetHeight() - y - 1) * Level.getMap().GetTileHeight(), Level.getMap().GetTileWidth(), Level.getMap().GetTileHeight(), new ItemSword());

//						Level.AddEntity(Ent);
					}
				}
			}

		} else
		{
			Populate(Rand, Level, Room.Left);
			Populate(Rand, Level, Room.Right);
		}
	}

}
