package ld_43.world.gen.def;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;

import ld_43.world.GameWorld;
import ld_43.world.Layer;
import ld_43.world.Location;
import ld_43.world.TiledWorld;
import ld_43.world.gen.RoomNode;
import ld_43.world.gen.WorldGen;
import ld_43.world.loader.tiled.TiledMap;

public class DefWorldGen extends WorldGen
{

	public DefWorldGen(int Width, int Height, Random Rand)
	{
		super(Width, Height, Rand, new DefPartitioner(), new DefRoomGen(), new DefStitcher(), new DefPopulator());
	}

	public boolean IsClear(TiledMap Map, int X, int Y)
	{
		boolean IsClear = true;
		Layer Foreground = Map.GetLayer("Foreground");
		for (int k = -1; k <= 1; k++)
		{
			for (int l = -1; l <= 1; l++)
			{
				int AX = X + k;
				int AY = Y + l;

				// Check if query is in bounds.
				if (AX >= 0 && AX < Foreground.Width && AY >= 0 && AY < Foreground.Height)
				{
					// Check if tile is empty.
					if (Foreground.GetTile(AX, AY) != null)
						IsClear = false;
				}
			}
		}

		return IsClear;

	}

	@Override
	public GameWorld GenerateLevel()
	{
		RoomNode RootNode = new RoomNode(0, 0, Width, Height);

		Pixmap Pixmap = new Pixmap((int) RootNode.Width, (int) RootNode.Height, Format.RGB888);
		Marked = new boolean[(int) RootNode.Width * (int) RootNode.Height];

		Partitioner.Partition(Rand, RootNode);

		// Generate rooms and map.
		TiledMap Map = RoomGen.GenRooms(Rand, RootNode, Marked);

		// Stitch generated rooms together.
		Stitcher.Stitch(Map, RootNode, Marked);

		GameWorld Lev = new GameWorld(Map, Marked, RootNode);

		// The player spawns in the bottom left leaf.
		RoomNode SpawnNode = RootNode;
		while (!SpawnNode.IsLeaf())
			SpawnNode = SpawnNode.Left;

		for (int i = 0; i < RootNode.Width; i++)
		{
			for (int j = 0; j < RootNode.Height; j++)
			{
				if (Map.GetLayer("Foreground").GetTile(i, j) == null)
				{
					Pixmap.drawPixel(i, j, 0xFF0000FF);
				}
				else
				{
					Pixmap.drawPixel(i, j, 0x0000FFFF);
				}
			}
		}

		for (int i = 0; i < SpawnNode.RoomW; i++)
		{
			for (int j = 0; j < SpawnNode.RoomH; j++)
			{

				if (IsClear(Map, SpawnNode.RoomX + i, SpawnNode.RoomY + j))
				{
					Lev.SetPlayerSpawn(new Location(SpawnNode.RoomX + i, Map.GetHeight() - (SpawnNode.RoomY + j) - 1));
					Pixmap.drawPixel(SpawnNode.RoomX + i, SpawnNode.RoomY + j, 0x00FF00FF);
				}
			}
		}

		// First room is completed by default.
		SpawnNode.Complete(Lev);

		// Populate the world with items.
		Populator.Populate(Rand, Lev, RootNode);

		PixmapIO.writePNG(Gdx.files.external("./Worlds/TestGen.png"), Pixmap);

		return Lev;
	}

}
