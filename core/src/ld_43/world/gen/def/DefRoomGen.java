package ld_43.world.gen.def;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;

import ld_43.world.Layer;
import ld_43.world.Tile;
import ld_43.world.gen.RoomGenerator;
import ld_43.world.gen.RoomNode;
import ld_43.world.loader.tiled.TiledMap;

public class DefRoomGen extends RoomGenerator
{

	private List<TiledMap> PreGens;
	public static final int FG_FILLER = 54;

	public int RandInt(Random Rand, int Min, int Max)
	{
		return (int) ((Rand.nextFloat()) * (Max - Min + 1)) + Min;
	}

	public DefRoomGen()
	{
		PreGens = new ArrayList<TiledMap>();
	}

	@Override
	public TiledMap GenRooms(Random Rand, RoomNode Partition, boolean[] Marked)
	{
		TiledMap Generated = new TiledMap((int) Partition.Width, (int) Partition.Height, 16, 16);

		// Load pregens
		PreGens.add(new TiledMap(Gdx.files.internal("./Worlds/1.tmx")));
		PreGens.add(new TiledMap(Gdx.files.internal("./Worlds/2.tmx")));
		PreGens.add(new TiledMap(Gdx.files.internal("./Worlds/3.tmx")));

		Layer BGLayer = new Layer("Background", (int) Partition.Width, (int) Partition.Height);
		Layer BG2Layer = new Layer("Background2", (int) Partition.Width, (int) Partition.Height);
		Layer FGLayer = new Layer("Foreground", (int) Partition.Width, (int) Partition.Height);

		// Fill foreground by default. Rooms will clear foreground.
		// In future this will copy over all layers from a premade room asset.

		Generated.AddLayer(FGLayer);
		Generated.AddLayer(BGLayer);
		Generated.AddLayer(BG2Layer);

		FillRooms(Rand, Generated, Partition, FGLayer, Marked);

		// Fill the empty parts of the foreground with the foreground filler
		int count = 0;
		for (int i = 0; i < FGLayer.Width; i++)
		{
			for (int j = 0; j < FGLayer.Height; j++)
			{
				Tile Tile = FGLayer.GetTile(i, j);
				if (Tile == null && !Marked[i + j * FGLayer.Width])
				{
					FGLayer.SetTile(i, j, Generated.GetTile(FG_FILLER));
				}
				else
				{
					count++;
				}
			}
		}
		return Generated;
	}

	private TiledMap PickRoom(Random Rand)
	{
		int Index = RandInt(Rand, 0, PreGens.size() - 1);

		return PreGens.get(Index);
	}

	private void Mark(Layer FG, int X, int Y, int W, int H, boolean[] Marked)
	{
		for (int i = X; i < X + W; i++)
		{
			for (int j = Y; j < Y + H; j++)
			{
				Marked[i + j * FG.Width] = true;
			}
		}
	}

	private void FillRooms(Random Rand, TiledMap RootMap, RoomNode Root, Layer FG, boolean[] Marked)
	{
		if (Root.Left == null && Root.Right == null)
		{
			// Randomly select a pregen'd room
			TiledMap PreGen = PickRoom(Rand); // this determines our width and height

			// Fill room with one tile padding around partition edges
			int Width = PreGen.GetWidth();
			int Height = PreGen.GetHeight();
			int StartX = RandInt(Rand, (int) Root.X + 1, (int) (Root.X + Root.Width) - Width - 2);
			int StartY = RandInt(Rand, (int) Root.Y + 1, (int) (Root.Y + Root.Height) - Height - 2);

			// Update the room information in the node.
			Root.RoomX = StartX;
			Root.RoomY = StartY;
			Root.RoomW = Width;
			Root.RoomH = Height;

			// Paste room
			RootMap.Paste(PreGen, StartX, StartY);

			// Mark this area as generated so we don't cover it later on
			Mark(FG, StartX, StartY, Width, Height, Marked);

		}
		else
		{
			FillRooms(Rand, RootMap, Root.Left, FG, Marked);
			FillRooms(Rand, RootMap, Root.Right, FG, Marked);
		}
	}

}
