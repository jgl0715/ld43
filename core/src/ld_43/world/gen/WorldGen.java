package ld_43.world.gen;

import java.util.Random;

import ld_43.world.GameWorld;

public abstract class WorldGen
{

	protected int Width;
	protected int Height;
	protected Partitioner Partitioner;
	protected RoomGenerator RoomGen;
	protected Stitcher Stitcher;
	protected Populator Populator;
	protected Random Rand;
	public boolean[] Marked;

	public WorldGen(int Width, int Height, Random Rand, Partitioner Part, RoomGenerator RoomGen, Stitcher Stitch, Populator Pop)
	{
		this.Width = Width;
		this.Height = Height;
		this.Partitioner = Part;
		this.RoomGen = RoomGen;
		this.Stitcher = Stitch;
		this.Populator = Pop;
		this.Rand = Rand;
	}

	public abstract GameWorld GenerateLevel();

	public Partitioner getPartitioner()
	{
		return Partitioner;
	}

	public RoomGenerator getRoomGen()
	{
		return RoomGen;
	}

}
