package ld_43.world;

public class Layer
{

	public int Width;
	public int Height;
	public Tile[] Tiles;
	public String Name;

	public Layer(String Name, int Width, int Height)
	{
		this.Name = Name;
		this.Width = Width;
		this.Height = Height;
		this.Tiles = new Tile[Width * Height];
	}

	public Tile GetTile(int X, int Y)
	{
		return Tiles[X + Y * Width];
	}

	public void SetTile(int X, int Y, Tile Tile)
	{
		Tiles[X + Y * Width] = Tile;
	}

	public void ClearTile(int X, int Y)
	{
		SetTile(X, Y, null);
	}

}
