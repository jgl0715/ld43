package ld_43.world;

import ld_43.entity.Entity;

public interface HitListener
{

	public void OnHitBegin(Entity Other);

	public void OnHitEnd(Entity Other);

}
