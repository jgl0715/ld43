package ld_43.world;

import ld_43.world.gen.RoomNode;
import ld_43.world.loader.tiled.TiledMap;

public class GameWorld extends TiledWorld
{

	private RoomNode Root;
	private Location PlayerSpawn;

	public GameWorld(TiledMap Map, boolean[] Marked, RoomNode Root)
	{
		super(Map, Marked);

		this.Root = Root;
		this.PlayerSpawn = new Location();
	}

	public Location GetPlayerSpawn()
	{
		return PlayerSpawn;
	}

	public void SetPlayerSpawn(Location playerSpawn)
	{
		PlayerSpawn = playerSpawn;
	}

	public int GetAmountUncompleted()
	{
		return GetAmountUncompleted(Root);
	}

	public int GetAmountUncompleted(RoomNode Root)
	{
		if (Root == null)
			return 0;
		else if (Root.IsComplete())
		{
			return 0 + GetAmountUncompleted(Root.Left) + GetAmountUncompleted(Root.Right);
		}
		else
		{
			return 1 + GetAmountUncompleted(Root.Left) + GetAmountUncompleted(Root.Right);
		}
	}

	public boolean IsLevelComplete()
	{
		return Root.IsComplete();
	}

	public boolean HasAccess(int TileX, int TileY)
	{
		RoomNode Node = GetLeaf(TileX, TileY);

		if (Node.Parent != null)
		{
			if (Node == Node.Parent.Right)
				return Node.Parent.Left.IsComplete();
			else
				return Node.IsComplete();
		}

		return true;

	}

	public RoomNode GetLeaf(int TileX, int TileY)
	{
		return GetLeaf(Root, TileX, TileY);
	}

	public RoomNode GetLeaf(RoomNode Root, int TileX, int TileY)
	{
		if (Root == null)
			return null;
		else if (Root.IsLeaf())
		{
			if (TileX >= Root.RoomX && TileY >= Root.RoomY && TileX < Root.RoomX + Root.RoomW && TileY < Root.RoomY + Root.RoomH)
				return Root;
			else
				return null;
		}
		else
		{
			RoomNode Left = GetLeaf(Root.Left, TileX, TileY);
			RoomNode Right = GetLeaf(Root.Right, TileX, TileY);

			if (Left != null)
				return Left;
			return Right;
		}
	}

}
