package ld_43.item;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import ld_43.Assets;
import ld_43.LD;

public class ItemSword extends Item
{

	public ItemSword()
	{
		super("Sword", new TextureRegion(LD.GetTexture(Assets.ITEMS), 0, 0, 32, 32));
	}

}
