package ld_43.item;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Item
{
	private String Name;
	private int ID;
	private TextureRegion Region;

	public Item(String Name, TextureRegion Region)
	{
		this.Name = Name;
		this.Region = Region;
	}
	
	public String GetName()
	{
		return Name;
	}

	public void SetName(String Name)
	{
		this.Name = Name;
	}

	public TextureRegion GetRegion()
	{
		return Region;
	}

	public void SetRegion(TextureRegion Region)
	{
		this.Region = Region;
	}

	public int GetID()
	{
		return ID;
	}

	public void SetID(int ID)
	{
		this.ID = ID;
	}
}
