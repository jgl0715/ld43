package ld_43;

import java.util.Stack;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import ld_43.input.ActionSource;
import ld_43.input.InputManager;
import ld_43.input.SourceType;
import ld_43.states.GameState;
import ld_43.states.LoadState;
import ld_43.states.MenuState;
import ld_43.states.OptionsState;
import ld_43.states.PlayState;
import ld_43.states.SetupState;

public class LD extends ApplicationAdapter
{

	public static final PlayState PLAY_STATE = new PlayState();
	public static final SetupState SETUP_STATE = new SetupState();
	public static final OptionsState OPTIONS_STATE = new OptionsState();
	public static final MenuState MENU_STATE = new MenuState();
	public static final LoadState LOAD_STATE = new LoadState();

	public static final int VIRTUAL_WIDTH = 16 * 40;
	public static final int VIRTUAL_HEIGHT = 9 * 40;

	private static LD Instance;
	private static SpriteBatch FGBatch;
	private static SpriteBatch BGBatch;
	private static AssetManager AssetManager;
	private static Settings Settings;
	private static Stack<GameState> GameStates;
	private static InputManager InputManager;

	public LD()
	{
		if (Instance != null)
			throw new IllegalStateException("Can only create one instance of this class");

		Instance = this;
	}

	@Override
	public void create()
	{

		// Create the settings.
		Settings = new Settings();

		// Create the input manager.
		InputManager = new InputManager();
		InputManager.RegisterAction(Inputs.MOVE_DOWN);
		InputManager.RegisterAction(Inputs.MOVE_UP);
		InputManager.RegisterAction(Inputs.MOVE_LEFT);
		InputManager.RegisterAction(Inputs.MOVE_RIGHT);
		InputManager.RegisterActionBinding(Inputs.MOVE_DOWN, new ActionSource(SourceType.KEYBOARD, Keys.S));
		InputManager.RegisterActionBinding(Inputs.MOVE_UP, new ActionSource(SourceType.KEYBOARD, Keys.W));
		InputManager.RegisterActionBinding(Inputs.MOVE_LEFT, new ActionSource(SourceType.KEYBOARD, Keys.A));
		InputManager.RegisterActionBinding(Inputs.MOVE_RIGHT, new ActionSource(SourceType.KEYBOARD, Keys.D));

		// Create the asset manager and queue all assets.
		AssetManager = new AssetManager();
		AssetManager.load(Assets.GDX_HEAD, Texture.class);
		AssetManager.load(Assets.SKIN, Skin.class);
		AssetManager.load(Assets.TILES, Texture.class);
		AssetManager.load(Assets.CAVE_TILES, Texture.class);
		AssetManager.load(Assets.ITEMS, Texture.class);
		AssetManager.load(Assets.CHARACTER, Texture.class);
		AssetManager.load(Assets.SFX_ACHIEVEMENT, Sound.class);
		AssetManager.load(Assets.SFX_ALARM, Sound.class);
		AssetManager.load(Assets.THEME1, Music.class);
		AssetManager.load(Assets.THEME2, Music.class);
		AssetManager.load(Assets.THEME3, Music.class);

		// Create the sprite batch.
		FGBatch = new SpriteBatch();
		BGBatch = new SpriteBatch();

		// Create the game state stack.
		GameStates = new Stack<GameState>();

		// Set the initial state to loading.
		SetState(LOAD_STATE);

	}

	@Override
	public void render()
	{

		// Update
		GameState Current = GetCurrentState();

		if (Current != null)
		{

			// Update the current game state.
			Current.Tick(Gdx.graphics.getDeltaTime());

			// Clear the screen and render the current game state.
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			Current.Render();
		}
	}

	@Override
	public void resize(int NewWidth, int NewHeight)
	{
		super.resize(NewWidth, NewHeight);

		GetCurrentState().Resized(NewWidth, NewHeight);
	}

	@Override
	public void dispose()
	{
		// Dispose all game states
		PLAY_STATE.Dispose();
		SETUP_STATE.Dispose();
		OPTIONS_STATE.Dispose();
		MENU_STATE.Dispose();
		LOAD_STATE.Dispose();

		// Remove all game states from the state stack.
		while (!GameStates.isEmpty())
			GameStates.pop();

		// Dispose all resources
		AssetManager.dispose();

		// Dispose sprite batch.
		FGBatch.dispose();
		BGBatch.dispose();

	}

	public static Sound GetSound(String AssetName)
	{
		return AssetManager.get(AssetName, Sound.class);
	}

	public static Music GetMusic(String AssetName)
	{
		return AssetManager.get(AssetName, Music.class);
	}

	public static Texture GetTexture(String AssetName)
	{
		return AssetManager.get(AssetName, Texture.class);
	}

	public static Skin GetSkin(String AssetName)
	{
		return AssetManager.get(AssetName, Skin.class);
	}

	public static GameState GetCurrentState()
	{
		if (GameStates.isEmpty())
			return null;
		else
			return GameStates.peek();
	}

	public static void SetState(GameState State)
	{
		// Clear all current game states on the stack.
		while (!GameStates.isEmpty())
			GameStates.pop();

		PushState(State);
	}

	public static void PushState(GameState State)
	{
		GameState Current = GetCurrentState();
		if (Current != null)
			Current.Hide();

		if (!State.IsCreated())
		{
			State.Create();
			State.Resized(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}

		GameStates.push(State);

		State.Show();
	}

	public static void PopState()
	{
		GameState Current = GetCurrentState();
		Current.Hide();

		GameStates.pop();

		Current = GetCurrentState();
		if (Current != null)
		{
			Current.Show();

			// Window may have resized while the state was hidden.
			Current.Resized(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}
	}

	public static Settings GetSettings()
	{
		return Settings;
	}

	public static SpriteBatch GetFGBatch()
	{
		return FGBatch;
	}

	public static InputManager GetInputManager()
	{
		return InputManager;
	}

	public static SpriteBatch GetBGBatch()
	{
		return BGBatch;
	}

	public static AssetManager GetAssetManager()
	{
		return AssetManager;
	}
}
