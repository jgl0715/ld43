package ld_43;

public class Assets
{
	public static final String TEXTURES_DIR = "./Textures";
	public static final String AUDIO_DIR = "./Audio";

	public static final String GDX_HEAD = "./badlogic.jpg";
	public static final String SKIN = "./Skins/default/uiskin.json";

	public static final String TILES = TEXTURES_DIR + "/Tiles.png";
	public static final String CAVE_TILES = TEXTURES_DIR + "/Cave_Tiles.png";
	public static final String ITEMS = TEXTURES_DIR + "/Items.png";
	

	public static final String CHARACTER = TEXTURES_DIR + "/Character.png";

	public static final String SFX_ACHIEVEMENT = AUDIO_DIR + "/SFX_Achievement.wav";
	public static final String SFX_ALARM = AUDIO_DIR + "/SFX_Alarm.wav";

	public static final String THEME1 = AUDIO_DIR + "/Theme1.mp3";
	public static final String THEME2 = AUDIO_DIR + "/Theme2.wav";
	public static final String THEME3 = AUDIO_DIR + "/Theme3.wav";

}
