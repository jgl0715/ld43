package ld_43;

public class Settings
{

	public float MasterVol;
	public float MusicVol;
	public float SFXVol;
	public boolean FullscreenEnabled;
	public int SavedWidth;
	public int SavedHeight;
	
	public Settings()
	{
		FullscreenEnabled = false;
		MasterVol = 1.0f;
		MusicVol = 1.0f;
		SFXVol = 1.0f;
	}

}
