package ld_43.entity.data;

import box2dLight.PointLight;
import ld_43.entity.Entity;

public class DPointLight extends Data
{
	
	public PointLight Value;

	public DPointLight(String Name, Entity Parent)
	{
		super(Name, Parent, false);
	}

}
