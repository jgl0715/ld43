package ld_43.entity;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ld_43.RenderSettings;
import ld_43.entity.behavior.Behavior;
import ld_43.entity.data.Data;
import ld_43.world.World;

public class Entity
{

	// Common component names
	public static final String POSITION = "Position";
	public static final String VELOCITY = "Velocity";
	public static final String SIZE = "Size";
	public static final String ROTATION = "Rotation";
	public static final String ACCELERATION = "Acceleration";
	public static final String FRAME = "Frame";
	public static final String BODY = "Body";

	private volatile static int nextId = 0;

	protected volatile int id;
	protected World world;
	protected RenderSettings RenderSettings;
	private EntityConfig Config;
	private Map<Integer, Data> DataComponents;
	private List<Behavior> BehaviorComponents;
	private int NextDataID;
	private boolean dead;

	/**
	 * All game objects must contain this constructor.
	 * 
	 * @param world
	 *            The world that the game object will be spawned in.
	 * @param server
	 *            Whether or not the game instance this game object is running on is
	 *            a server.
	 */
	public Entity(EntityConfig Config)
	{
		this.Config = Config;
		this.id = nextId++;
		this.world = Config.getWorld();
		this.RenderSettings = new RenderSettings();
		this.NextDataID = 0;
		this.DataComponents = new TreeMap<Integer, Data>();
		this.BehaviorComponents = new ArrayList<Behavior>();
		this.dead = false;
	}

	public EntityConfig GetConfig()
	{
		return Config;
	}

	public boolean IsDead()
	{
		return dead;
	}

	public void Remove()
	{
		dead = true;
	}
	
	public RenderSettings GetRenderSettings()
	{
		return RenderSettings;
	}

	public int GetId()
	{
		return id;
	}

	public World GetWorld()
	{
		return world;
	}

	public <T> void AddBehavior(Class<T> Class, String Name)
	{
		try
		{
			Constructor<?> CTor = Class.getConstructor(String.class, Entity.class);
			Behavior Component = (Behavior) CTor.newInstance(Name, this);
			AddBehavior(Component);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public <T> T AddData(Class<T> CompClass, String Name, boolean Serialized)
	{
		try
		{
			Constructor<?> CTor = CompClass.getConstructor(String.class, Entity.class, Boolean.TYPE);
			Data Component = (Data) CTor.newInstance(Name, this, Serialized);
			AddData(Component);

			return CompClass.cast(Component);
		} catch (NoSuchMethodException e)
		{
			System.err.println("Failed to find data component constructor. Maybe this component is not serialized?");
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public <T> T AddData(Class<T> CompClass, String Name)
	{
		try
		{
			Constructor<?> CTor = CompClass.getConstructor(String.class, Entity.class);
			Data Component = (Data) CTor.newInstance(Name, this);
			AddData(Component);

			return CompClass.cast(Component);
		} catch (NoSuchMethodException e)
		{
			System.err.println("Failed to find data component constructor. Maybe this component is serialized?");
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public void AddData(Data DataComponent)
	{
		DataComponents.put(NextDataID++, DataComponent);
	}

	public void AddBehavior(Behavior Behavior)
	{
		BehaviorComponents.add(Behavior);
	}

	public <T> T Find(Class<T> Class, String Name)
	{
		if (Data.class.isAssignableFrom(Class))
			return FindData(Class, Name);
		else if (Behavior.class.isAssignableFrom(Class))
			return FindBehavior(Class, Name);

		return null;
	}

	private <T> T FindData(Class<T> Class, String Name)
	{
		Iterator<Integer> ComponentItr = DataComponents.keySet().iterator();
		while (ComponentItr.hasNext())
		{
			int ComponentID = ComponentItr.next();
			Data Component = DataComponents.get(ComponentID);

			if (Component.getClass() == Class && Component.GetName().equalsIgnoreCase(Name))
				return Class.cast(Component);
		}

		return null;
	}

	private <T> T FindBehavior(Class<T> clazz, String name)
	{
		for (Behavior Component : BehaviorComponents)
		{
			if (Component.getClass() == clazz && Component.GetName().equalsIgnoreCase(name))
				return clazz.cast(Component);
		}
		return null;
	}

	public Data FindData(int DataID)
	{
		Iterator<Integer> ComponentItr = DataComponents.keySet().iterator();
		while (ComponentItr.hasNext())
		{
			int ComponentID = ComponentItr.next();
			if (ComponentID == DataID)
				return DataComponents.get(ComponentID);
		}

		return null;
	}

	public void Update(float delta)
	{
		for (Behavior Component : BehaviorComponents)
		{
			if (Component.DoesUpdate())
				Component.Update(delta);
		}
	}

	public void Render()
	{

	}

}
