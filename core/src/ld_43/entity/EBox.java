package ld_43.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import ld_43.LD;
import ld_43.RenderSettings.RenderMode;
import ld_43.entity.behavior.BPhysics;
import ld_43.entity.data.DBody;
import ld_43.entity.data.DFloat;
import ld_43.entity.data.DTextureRegion;
import ld_43.entity.data.DVector2;

public class EBox extends Entity
{

	private Vector2 Pos;
	private Vector2 Size;
	private DTextureRegion Frame;
	private DFloat Rot;

	public EBox(EntityConfig Config)
	{
		super(Config);

		Pos = AddData(DVector2.class, POSITION, true).Value;
		Size = AddData(DVector2.class, SIZE, false).Value;
		Rot = AddData(DFloat.class, ROTATION, true);
		Frame = AddData(DTextureRegion.class, FRAME);
		AddData(DBody.class, BODY);

		if (Config.HasItem("Width"))
			Size.x = Config.GetConfigNumber("Width");
		if (Config.HasItem("Height"))
			Size.y = Config.GetConfigNumber("Height");

		// Add a physics component
		AddBehavior(BPhysics.class, "Physics");

		RenderSettings.visible = true;
		RenderSettings.mode = RenderMode.TEXTURED;
	}

	public Vector2 GetPosition()
	{
		return Pos;
	}

	public Vector2 GetSize()
	{
		return Size;
	}

	@Override
	public void Update(float delta)
	{
		super.Update(delta);

		// System.out.println("entity position: " + Pos);
	}

	@Override
	public void Render()
	{
		super.Render();

		if (RenderSettings.visible)
		{
			switch (RenderSettings.mode)
			{
			case TEXTURED:

				SpriteBatch Batch = LD.GetBGBatch();
				float Wid = Size.x;
				float Hei = Size.y;
				float MidX = Wid / 2;
				float MidY = Hei / 2;

				Batch.draw(Frame.Value, Pos.x - MidX, Pos.y - MidY, MidX, MidY, Wid, Hei, RenderSettings.FlipX ? -1.0f : 1.0f, 1.0f, Rot.Value);

				break;
			case FRAME:

				// Use default entity texture scaled up here.

				// ShapeRenderer sr = EngineMain.getShapeRenderer();
				// Vector2f position = findDataComponentByClassAndName(DVector2.class,
				// POSITION).value;
				// Vector2f size = findDataComponentByClassAndName(DVector2.class, SIZE).value;
				// float rotation = findDataComponentByClassAndName(DFloat.class,
				// ROTATION).value;
				// float width = size.x;
				// float height = size.y;
				//
				// sr.begin(ShapeType.Line);
				// sr.rect(position.x - width / 2, position.y - height / 2, width / 2, height /
				// 2, width, height, 1.0f, 1.0f, rotation);
				// sr.end();
				//
				// break;
			default:
				break;
			}
		}

	}

}
