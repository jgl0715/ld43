package ld_43.input;

import com.badlogic.gdx.Gdx;

public class AxisSource
{

	private SourceType Source;
	private int ID;
	private float Multiplier;

	public AxisSource(SourceType Source, int ID)
	{
		this.Source = Source;
		this.ID = ID;
		SetMultiplier(1);
	}

	public float GetMultiplier()
	{
		return Multiplier;
	}

	public void SetMultiplier(float Multiplier)
	{
		this.Multiplier = Multiplier;
	}

	public float GetValue()
	{
		switch (Source)
		{
		case KEYBOARD:
			if (Gdx.input.isKeyPressed(ID))
				return Multiplier;
			else
				return 0;
		case MOUSE:
			if (ID == Axis.MOUSE_X)
				return Gdx.input.getX();
			else if (ID == Axis.MOUSE_Y)
				return Gdx.input.getY();
			else if (Gdx.input.isButtonPressed(ID))
				return Multiplier;
			else
				return 0;
		default:
			return 0;
		}
	}

}
