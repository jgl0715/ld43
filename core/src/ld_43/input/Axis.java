package ld_43.input;

public class Axis
{

	public static final int MOUSE_X = 0xFF0;
	public static final int MOUSE_Y = 0xFF1;

	public static boolean IsAxis(int Axis)
	{
		return !toString(Axis).equals("UNKNOWN");
	}

	public static String toString(int ID)
	{
		if (ID == MOUSE_X)
			return "MOUSE X";
		else if (ID == MOUSE_Y)
			return "MOUSE Y";
		else
			return "UNKNOWN";
	}

}
