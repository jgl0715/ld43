package ld_43.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;

public class ActionSource
{

	private SourceType Source;
	private int ID;

	public ActionSource(SourceType Source, int ID)
	{
		this.Source = Source;
		this.ID = ID;
	}

	public SourceType GetSource()
	{
		return Source;
	}

	public int GetId()
	{
		return ID;
	}
	
	public void SetSource(SourceType Source)
	{
		this.Source = Source;
	}
	
	public void SetID(int ID)
	{
		this.ID = ID;
	}

	@Override
	public String toString()
	{
		if (Source == SourceType.KEYBOARD)
			return Keys.toString(ID);
		else
		{
			if (ID == Buttons.LEFT)
				return "Left Mouse Button";
			else if (ID == Buttons.RIGHT)
				return "Right Mouse Button";
			else if (ID == Buttons.MIDDLE)
				return "Middle Mouse Button";
			else
				return "UNKNOWN";
		}
	}

	public boolean IsActivated()
	{
		switch (Source)
		{
		case KEYBOARD:
			return Gdx.input.isKeyPressed(ID);
		case MOUSE:
			return Gdx.input.isButtonPressed(ID);
		default:
			return false;
		}
	}

}
