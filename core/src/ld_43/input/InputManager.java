package ld_43.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

public class InputManager implements InputProcessor
{

	private List<String> Actions;
	private List<String> Axes;

	private Map<String, List<ActionSource>> ActionBinds;
	private Map<String, List<AxisSource>> AxisBinds;

	public InputManager()
	{
		Actions = new ArrayList<String>();
		Axes = new ArrayList<String>();

		ActionBinds = new HashMap<String, List<ActionSource>>();
		AxisBinds = new HashMap<String, List<AxisSource>>();
	}

	public void RegisterAction(String ActionName)
	{
		if (Actions.contains(ActionName))
			throw new IllegalStateException("Action " + ActionName + " already registered!");
		Actions.add(ActionName);
		ActionBinds.put(ActionName, new ArrayList<ActionSource>());
	}

	public void RegisterAxis(String AxisName)
	{
		if (Axes.contains(AxisName))
			throw new IllegalStateException("Axis " + AxisName + " already registered!");
		Axes.add(AxisName);
		AxisBinds.put(AxisName, new ArrayList<AxisSource>());
	}

	public void RegisterActionBinding(String Action, ActionSource Input)
	{
		// TODO: make custom exception handling and logging system
		if (!Actions.contains(Action))
			throw new IllegalStateException("Action " + Action + " not registered!");
		else
			ActionBinds.get(Action).add(Input);
	}

	public void RegisterAxisBinding(String Axis, AxisSource Input)
	{
		if (!Axes.contains(Axis))
			throw new IllegalStateException("Action " + Axis + " not registered!");
		else
			AxisBinds.get(Axis).add(Input);
	}

	public List<ActionSource> GetActionSources(String ActionBind)
	{
		return ActionBinds.get(ActionBind);
	}
	
	public List<AxisSource> GetAxisSources(String AxisBind)
	{
		return AxisBinds.get(AxisBind);
	}

	public Iterator<String> GetActionsIterator()
	{
		return ActionBinds.keySet().iterator();
	}

	public Iterator<String> GetAxisIterator()
	{
		return AxisBinds.keySet().iterator();
	}

	public boolean IsActionActivated(String ActionName)
	{
		List<ActionSource> ActionSources = ActionBinds.get(ActionName);

		// Check all input sources registered with this action.
		for (ActionSource Source : ActionSources)
			if (Source.IsActivated())
				return true;
		return false;
	}

	public float GetAxisValue(String AxisName)
	{
		List<AxisSource> AxisSources = AxisBinds.get(AxisName);

		// Check all input sources registered with this action.
		float Sum = 0.0f;
		for (AxisSource Source : AxisSources)
			Sum += Source.GetValue();

		return Sum;
	}

	public Vector2 MouseVector(int screenX, int screenY)
	{
		return new Vector2(Gdx.input.getX() - screenX, (Gdx.graphics.getHeight() - Gdx.input.getY()) - screenY);
	}

	public float AngleFrom(int screenX, int screenY)
	{
		Vector2 mouseVec = MouseVector(screenX, screenY);

		return (float) Math.atan2(mouseVec.y, mouseVec.x);
	}

	@Override
	public boolean keyDown(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		return false;
	}

}
