package ld_43.states;

import ld_43.LD;

public class LoadState extends GameState
{

	@Override
	public void Create()
	{
		super.Create();
	}

	@Override
	public void Tick(float Delta)
	{
		// We're done updating, we can move onto the menu screen now.
		if(LD.GetAssetManager().update())
			LD.SetState(LD.MENU_STATE);
	}

	@Override
	public void Render()
	{
		// Render maybe a progress bar
	}

	@Override
	public void Hide()
	{

	}

	@Override
	public void Show()
	{

	}

	@Override
	public void Resized(int NewWidth, int NewHeight)
	{

	}

	@Override
	public void Dispose()
	{

	}

}
