package ld_43.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import ld_43.Assets;
import ld_43.LD;

public class SetupState extends GameState
{

	private Stage Scene;
	private OrthographicCamera Camera;
	private Viewport Viewport;

	private Label SeedLabel;
	private TextField SeedField;
	private CheckBox NormalModeBox;
	private CheckBox TimeTrialModeBox;
	private TextButton StartButton;
	private TextButton BackButton;

	@Override
	public void Create()
	{
		super.Create();

		Camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Viewport = new StretchViewport(LD.VIRTUAL_WIDTH, LD.VIRTUAL_HEIGHT, Camera);
		Scene = new Stage(Viewport, LD.GetFGBatch());
		Skin Skin = LD.GetSkin(Assets.SKIN);

		// Layout and create widgets.
		Table Root = new Table();
		Root.setFillParent(true);
		Root.setDebug(true, true);
		{
			Table UIGroup = new Table();
			UIGroup.defaults().pad(10).width(300);
			{

				Table SeedUI = new Table();
				{
					SeedLabel = new Label("Seed", Skin, "default-font", Color.WHITE);
					SeedField = new TextField("", LD.GetSkin(Assets.SKIN));

					SeedUI.add(SeedLabel).padRight(50);
					SeedUI.add(SeedField).width(200);

					UIGroup.add(SeedUI).row();
				}

				Table ModeUI = new Table();
				{
					NormalModeBox = new CheckBox("Normal", Skin);
					TimeTrialModeBox = new CheckBox("Time Trial", Skin);

					NormalModeBox.setChecked(true);
					TimeTrialModeBox.setChecked(false);

					NormalModeBox.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							if (TimeTrialModeBox.isChecked())
								TimeTrialModeBox.setChecked(false);
							else
								NormalModeBox.setChecked(true);
						}
					});

					TimeTrialModeBox.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							if (NormalModeBox.isChecked())
								NormalModeBox.setChecked(false);
							else
								TimeTrialModeBox.setChecked(true);
						}
					});

					ModeUI.add(NormalModeBox).padRight(100);
					ModeUI.add(TimeTrialModeBox);

					UIGroup.add(ModeUI).row();
				}

				Table ButtonUI = new Table();
				ButtonUI.defaults().width(150).pad(5);
				{
					StartButton = new TextButton("Start", Skin);
					BackButton = new TextButton("Back", Skin);

					StartButton.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							// Start new game with specified seed.
							LD.PLAY_STATE.getRandom().setSeed(SeedField.hashCode());
							LD.SetState(LD.PLAY_STATE);

						}
					});

					BackButton.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							LD.PopState();
						}
					});

					ButtonUI.add(StartButton);
					ButtonUI.add(BackButton);

					UIGroup.add(ButtonUI).row();
				}

			}

			Root.add(UIGroup).center();
		}

		Scene.addActor(Root);
	}

	@Override
	public void Tick(float Delta)
	{
		Scene.act(Delta);
	}

	@Override
	public void Render()
	{
		Scene.draw();
	}

	@Override
	public void Hide()
	{
		SeedField.setText("");
		NormalModeBox.setChecked(true);
		TimeTrialModeBox.setChecked(false);

		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void Show()
	{
		Gdx.input.setInputProcessor(Scene);
	}

	@Override
	public void Resized(int NewWidth, int NewHeight)
	{
		Viewport.update(NewWidth, NewHeight);
	}

	@Override
	public void Dispose()
	{

	}

}
