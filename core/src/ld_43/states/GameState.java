package ld_43.states;

public abstract class GameState
{
	
	private boolean Created;
	
	public GameState()
	{
		Created = false;
	}
	
	public boolean IsCreated()
	{
		return Created;
	}
	
	public void Create()
	{
		Created = true;
	}
	
	public abstract void Tick(float Delta);
	public abstract void Render();
	public abstract void Hide();
	public abstract void Show();
	public abstract void Resized(int NewWidth, int NewHeight);
	public abstract void Dispose();

}
