package ld_43.states;

import java.util.Random;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import ld_43.LD;
import ld_43.entity.EPlayer;
import ld_43.entity.EntityFactory;
import ld_43.world.GameWorld;
import ld_43.world.Location;
import ld_43.world.gen.def.DefWorldGen;

public class PlayState extends GameState
{

	private Random Random;
	private GameWorld Level;
	private OrthographicCamera Camera;
	private Viewport Viewport;
	private EPlayer Player;
	private Debug dbg;

	public PlayState()
	{
		Random = new Random();

	}

	public Random getRandom()
	{
		return Random;
	}

	@Override
	public void Create()
	{
		super.Create();

		Camera = new OrthographicCamera();
		Viewport = new StretchViewport(LD.VIRTUAL_WIDTH, LD.VIRTUAL_HEIGHT, Camera);
		dbg = new Debug();

		DefWorldGen WorldGen = new DefWorldGen(100, 100, Random);
		Level = WorldGen.GenerateLevel();

		// Spawn player.
		Location Spawn = Level.GetPlayerSpawn();

		if (Spawn != null)
		{
			Player = EntityFactory.CreatePlayer(Level, Spawn.X * Level.getMap().GetTileWidth(), Spawn.Y * Level.getMap().GetTileHeight());
			Level.AddEntity(Player);
		}

	}

	@Override
	public void Tick(float Delta)
	{
		Level.Update(Delta);

		Vector2 Pos = Player.GetPosition();
		Vector2 Size = Player.GetSize();

		Camera.position.set(Pos.x + Size.x / 2, Pos.y + Size.y / 2, 0);

		if (Camera.position.x < LD.VIRTUAL_WIDTH / 2)
			Camera.position.x = LD.VIRTUAL_WIDTH / 2;
		if (Camera.position.x > Level.getMap().GetPixelWidth() - LD.VIRTUAL_WIDTH / 2)
			Camera.position.x = Level.getMap().GetPixelWidth() - LD.VIRTUAL_WIDTH / 2;

		if (Camera.position.y < LD.VIRTUAL_HEIGHT / 2)
			Camera.position.y = LD.VIRTUAL_HEIGHT / 2;
		if (Camera.position.y > Level.getMap().GetPixelHeight() - LD.VIRTUAL_HEIGHT / 2)
			Camera.position.y = Level.getMap().GetPixelHeight() - LD.VIRTUAL_HEIGHT / 2;

		Camera.update();

		dbg.Update();
	}

	@Override
	public void Render()
	{
		Level.Render(Camera);
		dbg.Render();
	}

	public void Hide()
	{

	}

	public void Show()
	{

	}

	public void Resized(int NewWidth, int NewHeight)
	{
		Viewport.update(NewWidth, NewHeight);
		Level.Resize(NewWidth, NewHeight);
	}

	@Override
	public void Dispose()
	{

	}

}
