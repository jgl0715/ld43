package ld_43.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import ld_43.Assets;
import ld_43.LD;

public class Debug
{

	private Stage Scene;
	private OrthographicCamera Camera;
	private Viewport Viewport;

	private Label fpsLabel;

	public Debug()
	{
		Camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Viewport = new StretchViewport(LD.VIRTUAL_WIDTH, LD.VIRTUAL_HEIGHT, Camera);
		Scene = new Stage(Viewport, LD.GetFGBatch());
		Skin Skin = LD.GetSkin(Assets.SKIN);

		Table Root = new Table();
		Root.setFillParent(true);
		Root.setDebug(true, true);
		Root.top().left();
		{
			fpsLabel = new Label("", Skin);
			Root.add(fpsLabel);
		}
		
		Scene.addActor(Root);
	}
	
	public void Resize(int NewWidth, int NewHeight)
	{
		Viewport.update(NewWidth, NewHeight);
	}

	public void Update()
	{
		fpsLabel.setText("FPS: " + Gdx.graphics.getFramesPerSecond());

		Scene.act();
	}

	public void Render()
	{
		Scene.draw();
	}

}
