package ld_43.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import ld_43.Assets;
import ld_43.Inputs;
import ld_43.LD;

public class MenuState extends GameState
{

	private Stage Scene;
	private OrthographicCamera Camera;
	private Viewport Viewport;

	private TextButton PlayButton;
	private TextButton OptionsButton;
	private TextButton QuitButton;

	@Override
	public void Create()
	{
		super.Create();

		Camera = new OrthographicCamera();
		Viewport = new StretchViewport(LD.VIRTUAL_WIDTH, LD.VIRTUAL_HEIGHT, Camera);
		Scene = new Stage(Viewport, LD.GetFGBatch());

		// Layout and create widgets.
		Table Root = new Table();
		Root.setFillParent(true);
		Root.setDebug(true, true);
		{
			Table ButtonGroup = new Table();
			ButtonGroup.defaults().pad(10).width(300).height(40);
			{
				PlayButton = new TextButton("Play", LD.GetSkin(Assets.SKIN));
				OptionsButton = new TextButton("Options", LD.GetSkin(Assets.SKIN));
				QuitButton = new TextButton("Quit", LD.GetSkin(Assets.SKIN));

				PlayButton.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						super.clicked(event, x, y);
						
						LD.PushState(LD.SETUP_STATE);
					}
				});
				
				OptionsButton.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						super.clicked(event, x, y);
						
						LD.PushState(LD.OPTIONS_STATE);
					}
				});
				
				QuitButton.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						super.clicked(event, x, y);
						
						Gdx.app.exit();
					}
				});

				ButtonGroup.add(PlayButton).row();
				ButtonGroup.add(OptionsButton).row();
				ButtonGroup.add(QuitButton).row();
			}

			Root.add(ButtonGroup).center();
		}

		Scene.addActor(Root);
		
		LD.GetMusic(Assets.THEME2).play();
		LD.GetMusic(Assets.THEME2).setLooping(true);
		
	}

	@Override
	public void Tick(float Delta)
	{
		Scene.act(Delta);
		
	}

	@Override
	public void Render()
	{
		Scene.draw();
	}

	public void Hide()
	{

	}

	public void Show()
	{
		Gdx.input.setInputProcessor(Scene);
	}

	public void Resized(int NewWidth, int NewHeight)
	{
		Viewport.update(NewWidth, NewHeight);
	}

	@Override
	public void Dispose()
	{
		Scene.dispose();
	}

}
