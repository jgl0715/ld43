package ld_43.states;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import ld_43.Assets;
import ld_43.LD;
import ld_43.input.ActionSource;
import ld_43.input.SourceType;

public class OptionsState extends GameState implements InputProcessor
{

	private Stage Scene;
	private OrthographicCamera Camera;
	private Viewport Viewport;

	private Label MasterVolLabel;
	private Label MusicVolLabel;
	private Label SFXVolLabel;
	private Slider MasterVolSlider;
	private Slider MusicVolSlider;
	private Slider SFXVolSlider;

	private TextButton FullscreenButton;
	private TextButton BackButton;

	private TextButton ModifyingButton;
	private ActionSource ModifyingAction;
	private String ModifyingBind;

	private InputMultiplexer Mux;

	public OptionsState()
	{
		ModifyingAction = null;
		ModifyingButton = null;
	}

	@Override
	public void Create()
	{
		super.Create();

		Camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Viewport = new StretchViewport(LD.VIRTUAL_WIDTH, LD.VIRTUAL_HEIGHT, Camera);
		Scene = new Stage(Viewport, LD.GetFGBatch());

		Skin Skin = LD.GetSkin(Assets.SKIN);
		Mux = new InputMultiplexer(this, Scene);

		Table Root = new Table();
		Root.setFillParent(true);
		{

			// Left side options.
			Table LeftSettings = new Table();
			LeftSettings.defaults().padBottom(50);
			{
				Table AudioSettingsTable = new Table();
				AudioSettingsTable.defaults().pad(0);
				AudioSettingsTable.left().pad(10);
				{
					MasterVolLabel = new Label("Master", Skin);
					MusicVolLabel = new Label("Music", Skin);
					SFXVolLabel = new Label("SFX", Skin);

					MasterVolSlider = new Slider(0, 1, 0.001f, false, Skin);
					MusicVolSlider = new Slider(0, 1, 0.001f, false, Skin);
					SFXVolSlider = new Slider(0, 1, 0.001f, false, Skin);

					MasterVolSlider.setValue(LD.GetSettings().MasterVol);
					MusicVolSlider.setValue(LD.GetSettings().MusicVol);
					SFXVolSlider.setValue(LD.GetSettings().SFXVol);

					MasterVolSlider.addListener(new ChangeListener()
					{
						@Override
						public void changed(ChangeEvent event, Actor actor)
						{
							LD.GetSettings().MasterVol = MasterVolSlider.getValue();
						}
					});

					MusicVolSlider.addListener(new ChangeListener()
					{
						@Override
						public void changed(ChangeEvent event, Actor actor)
						{
							LD.GetSettings().MusicVol = MusicVolSlider.getValue();
						}
					});

					SFXVolSlider.addListener(new ChangeListener()
					{
						@Override
						public void changed(ChangeEvent event, Actor actor)
						{
							LD.GetSettings().SFXVol = SFXVolSlider.getValue();
						}
					});

					AudioSettingsTable.add(MasterVolLabel).left();
					AudioSettingsTable.add(MasterVolSlider).row();
					AudioSettingsTable.add(MusicVolLabel).left();
					AudioSettingsTable.add(MusicVolSlider).row();
					AudioSettingsTable.add(SFXVolLabel).left();
					AudioSettingsTable.add(SFXVolSlider).row();

					LeftSettings.add(AudioSettingsTable).row();
				}

				Table BottomSettingsTable = new Table();
				BottomSettingsTable.defaults().width(190).pad(10);
				BottomSettingsTable.left().pad(10);
				{
					FullscreenButton = new TextButton("Enable Fullscreen", Skin);

					FullscreenButton.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							if (LD.GetSettings().FullscreenEnabled)
							{
								FullscreenButton.setText("Enable Fullscreen");
								LD.GetSettings().FullscreenEnabled = false;

								Gdx.graphics.setWindowedMode(LD.GetSettings().SavedWidth, LD.GetSettings().SavedHeight);
							} else
							{
								FullscreenButton.setText("Disable Fullscreen");
								LD.GetSettings().FullscreenEnabled = true;

								LD.GetSettings().SavedWidth = Gdx.graphics.getWidth();
								LD.GetSettings().SavedHeight = Gdx.graphics.getHeight();

								Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayModes()[0]);
							}

						}
					});

					BackButton = new TextButton("Back", Skin);

					BackButton.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							LD.PopState();
						}
					});

					BottomSettingsTable.add(FullscreenButton).row();
					BottomSettingsTable.add(BackButton);

					LeftSettings.add(BottomSettingsTable).row();
				}
			}

			// Right side options.
			Table KeybindsTable = new Table();
			KeybindsTable.padRight(10);
			KeybindsTable.top();
			{
				// Load keybindings.
				Iterator<String> ActionBinds = LD.GetInputManager().GetActionsIterator();

				while (ActionBinds.hasNext())
				{
					final String Bind = ActionBinds.next();
					final ActionSource Source = LD.GetInputManager().GetActionSources(Bind).get(0);
					final TextButton Button = new TextButton(Bind + ": " + Source.toString(), Skin);

					Button.addListener(new ClickListener()
					{
						@Override
						public void clicked(InputEvent event, float x, float y)
						{
							super.clicked(event, x, y);

							ModifyingAction = Source;
							ModifyingButton = Button;
							ModifyingBind = Bind;
							
							ModifyingButton.setText("(Press Key)");
						}
					});

					KeybindsTable.add(Button).width(150).pad(3).row();
				}

			}

			Root.add(LeftSettings).expand().left().top();
			Root.add(KeybindsTable).expand().right().top();

		}

		Scene.addActor(Root);

	}

	@Override
	public void Tick(float Delta)
	{
		Scene.act(Delta);
	}

	@Override
	public void Render()
	{
		Scene.draw();
	}

	public void Hide()
	{

	}

	public void Show()
	{
		Gdx.input.setInputProcessor(Mux);
	}

	public void Resized(int NewWidth, int NewHeight)
	{
		Viewport.update(NewWidth, NewHeight);
	}

	@Override
	public void Dispose()
	{

	}

	@Override
	public boolean keyDown(int keycode)
	{
		if (ModifyingAction != null)
		{
			ModifyingAction.SetSource(SourceType.KEYBOARD);
			ModifyingAction.SetID(keycode);
			ModifyingButton.setText(ModifyingBind + ": " + ModifyingAction.toString());
			ModifyingAction = null;
			ModifyingButton = null;
			ModifyingBind = null;

			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		if (ModifyingAction != null)
		{
			ModifyingAction.SetSource(SourceType.MOUSE);
			ModifyingAction.SetID(button);
			ModifyingButton.setText(ModifyingBind + ": " + ModifyingAction.toString());

			ModifyingAction = null;
			ModifyingButton = null;
			ModifyingBind = null;

			return true;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		return false;
	}

}
