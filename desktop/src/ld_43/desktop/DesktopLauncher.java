package ld_43.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ld_43.LD;

public class DesktopLauncher
{

	public static void main(String[] arg)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 16 * 90;
		config.height = 9 * 90;
		new LwjglApplication(new LD(), config);
	}

}
